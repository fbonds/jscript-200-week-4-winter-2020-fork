// MATT/TIM: I had to use the solution to figure out how to do this.  
// I went over it line to be sure I understood it though.
// Somethings I didn't understand:
// 1. Why does getDeck() work when it's not implicitly 'imported'/'required' from creatCardDeck.js? 
//    If I were to run the program directly independent of the html file, it wouldn't would it? Is this good practice to rely on the html script tags? curious. thx.
// 2. Why isn't the deck being shrunk as cards are drawn?  How is it avoiding drawing the same card twice? (I think I solved this below)
// 3. Blackjack rules: Doesn't the house (aka dealer) win in a tie? 
// 4. What is the purpose of blackjack.spec.js? 


const blackjackDeck = getDeck();

/**
 * Represents a card player (including dealer).
 * @constructor
 * @param {string} name - The name of the player
 */
class CardPlayer {
  constructor(name) {
    this.name = name;
    this.hand = [];
  }

  drawCard() {

    // Matt/Tim: The cards have to be removed from the deck as played don't they? 
    let index = Math.floor( Math.random()*blackjackDeck.length );
    const card = blackjackDeck[index]; 
    this.hand.push(card);
    blackjackDeck.splice( index, 1 );
  }
}



// CREATE TWO NEW CardPlayers
const dealer = new CardPlayer('Dealer');
const player = new CardPlayer('Player');

/**
 * Calculates the score of a Blackjack hand
 * @param {Array} hand - Array of card objects with val, displayVal, suit properties
 * @returns {Object} blackJackScore
 * @returns {number} blackJackScore.total
 * @returns {boolean} blackJackScore.isSoft
 */
const calcPoints = function(hand) {
  let handScore = 0;
  
  let hasAce = false;

  let isSoft = false;

  for (let i = 0; i < hand.length; i++) {
    
    const card = hand[i];

    if (card.displayVal === 'Ace') {
    
      hasAce = true;
      handScore += 1;
    } else if (card.displayVal != 'Ace') {
      handScore += card.val;
    }
  }
  if (handScore <= 11 && hasAce) {
    handScore += 10;
    isSoft = true;
  }
  return {
    total: handScore,
    isSoft: isSoft
  };
};


/**
 * Determines whether the dealer should draw another card.
 * 
 * @param {Array} dealerHand Array of card objects with val, displayVal, suit properties
 * @returns {boolean} whether dealer should draw another card
 */
const dealerShouldDraw = function(dealerHand) {
  let points = calcPoints(dealerHand).total;
  let isSoft = calcPoints(dealerHand).isSoft;
  if (points < 17 || (points === 17 && isSoft)) {
    return true;
  } else if (points >= 17) {
    return false;
  }
};

/**
 * Determines the winner if both player and dealer stand
 * @param {number} playerScore 
 * @param {number} dealerScore 
 * @returns {string} Shows the player's score, the dealer's score, and who wins
 */
const determineWinner = function(playerScore, dealerScore) {
  if (playerScore > dealerScore) {
    return `Your total of ${playerScore} beat the dealer's total of ${dealerScore}.  You win!`;
  } else if (playerScore < dealerScore) {
    return `The dealer's total of ${dealerScore} beat your total of ${playerScore}.  You lose!`;
  } else if (playerScore === dealerScore) {
    return `The dealer's total of ${dealerScore} equaled your total of ${playerScore}.  Push!  House wins!`;
  }
};

/**
 * Creates user prompt to ask if they'd like to draw a card
 * @param {number} count 
 * @param {string} dealerCard 
 */
const getMessage = (count, dealerCard) => {
  return `Dealer showing ${dealerCard.displayVal}, your count is ${count}.  Draw card?`
}

/**
 * Logs the player's hand to the console
 * @param {CardPlayer} player 
 */
const showHand = function(player) {
  let displayHand = player.hand.map(function(card) {
    return card.displayVal;
  });
  console.log(
    `${player.name}'s hand is ${displayHand.join(', ')} (${
      calcPoints(player.hand).total
    })`
  );
};

/**
 * Runs Blackjack Game
 */
const startGame = function() {
  player.drawCard();
  dealer.drawCard();
  player.drawCard();
  dealer.drawCard();

  let playerScore = calcPoints(player.hand).total;
  showHand(player);
  while (playerScore < 21 && confirm(getMessage(playerScore, dealer.hand[0]))) {
    player.drawCard();
    playerScore = calcPoints(player.hand).total;
    showHand(player);
  }
  if (playerScore > 21) {
    return 'You went over 21 - you lose!';
  }
  console.log(`Player stands at ${playerScore}`);
  showHand(dealer);
  while (dealerShouldDraw(dealer.hand)) {
    dealer.drawCard();
    showHand(dealer);
  }
  let dealerScore = calcPoints(dealer.hand).total;
  if (dealerScore > 21) {
    return 'Dealer went over 21, you win!';
  } else {
    console.log(`Dealer stays at ${dealerScore}`);
  }

  return determineWinner(playerScore, dealerScore);
};
console.log(startGame());